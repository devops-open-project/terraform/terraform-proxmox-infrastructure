resource "proxmox_lxc" "lxc" {
    for_each = {
        for lxc in var.lxc_object : lxc.name => lxc
    }
    features {
        nesting = true
    }
    // Terraform will crash without rootfs defined
    rootfs {
      storage = each.value.storage
      size    = each.value.size
    }
    
    cores=each.value.cores
    memory=each.value.memory
    swap=each.value.swap
    hostname = each.value.name
    
    network {
        name = each.value.network_name
        bridge = each.value.bridge
        ip = each.value.ip
        gw = each.value.gw
    }
    nameserver = "8.8.8.8"
    ostemplate = each.value.ostemplate
    target_node = each.value.target_node
    unprivileged = true
    onboot = true
    ssh_public_keys = each.value.ssh_public_keys
    start = true
}
