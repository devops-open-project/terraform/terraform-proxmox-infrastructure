#!/bin/sh
if [ $(id -u) != 0 ]; then
  sudo -i $(realpath "$0") "$@"
  exit
fi
