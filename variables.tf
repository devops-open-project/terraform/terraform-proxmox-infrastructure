variable "lxc_object" {
  description = "lxc details"
  type = list(object({
    name            = string
    size            = string
    cores           = number
    memory          = number
    swap            = number
    storage         = string
    ostemplate      = string
    target_node     = string
    ssh_public_keys = string
    network_name    = string
    bridge          = string
    ip              = string
    gw              = string
  }))
  default = []
}

variable "vm_object" {
  description = "lxc details"
  type = list(object({
    name            = string
    clone           = string
    size            = string
    cores           = number
    sockets         = number
    memory          = number
    storage         = string
    target_node     = string
    ipconfig        = string
    sshkeys         = string
    user            = string
  }))
  default = []
}
