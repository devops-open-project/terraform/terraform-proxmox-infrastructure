resource "proxmox_vm_qemu" "vm" {
    for_each = {
        for vm in var.vm_object : vm.name => vm
    }
    ciuser=each.value.user
    name = each.value.name

    # Node name has to be the same name as within the cluster
    # this might not include the FQDN
    target_node = each.value.target_node

    # The template name to clone this vm from
    clone = each.value.clone

    os_type = "cloud-init"
    cores = each.value.cores
    sockets = each.value.sockets
    memory = each.value.memory
    boot     = "c"
    sshkeys  = each.value.sshkeys

    # Setup the disk
    disk {
        size = each.value.size
        type = "scsi"
        storage = each.value.storage
    }

    network {
        model = "virtio"
        bridge = "vmbr0"
    }
    # Setup the ip address using cloud-init.
    # Keep in mind to use the CIDR notation for the ip.
    ipconfig0 = each.value.ipconfig
}
